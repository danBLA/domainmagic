# -*- coding: UTF-8 -*-
#   Copyright 2012-2022 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#

"""ip tools"""

from domainmagic.validators import is_ipv4, is_ipv6
from domainmagic.fileupdate import updatefile
import stat
import ipaddress

try:
    import geoip2.database
    PYGEOIP_AVAILABLE = True
except ImportError:
    PYGEOIP_AVAILABLE = False


GEOIP_FILE = '/tmp/GeoLite2-Country.mmdb'
GEOIP_URL = 'https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz'


def ip6_expand(ip:str) -> str:
    """
    remove :: shortcuts from ip adress - the returned address has 8 parts
    deprecated, better use ipaddress directly
    """
    if '.' in ip:
        raise ValueError()
    
    addr = ipaddress.ip_address(ip)
    return addr.exploded


def ip_reversed(ip:str) -> str:
    """Return the reversed ip address representation for dns lookups"""
    addr = ipaddress.ip_address(ip)
    revptr = addr.reverse_pointer
    items = revptr.split('.')
    return '.'.join(items[:-2])


def convert_hex_ip(ip:str) -> str:
    ipbytes = ip.split('.')
    newbytes = []
    for ipbyte in ipbytes:
        if ipbyte.startswith('0x'):
            try:
                ipbyte = str(int(ipbyte, 0))
                newbytes.append(ipbyte)
            except ValueError:
                raise ValueError("invalid ip address: %s" % ip)
        else:
            newbytes.append(ipbyte)
    return '.'.join(newbytes)


@updatefile(GEOIP_FILE, GEOIP_URL, refresh_time=24*3600, minimum_size=1000, unpack=True,
            filepermission=stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IROTH)
def geoip_country_code_by_addr(ip):
    assert PYGEOIP_AVAILABLE, "geoip2 is not installed"
    gi = geoip2.database.Reader(GEOIP_FILE)
    data = gi.country(ip)
    retval = data.country.iso_code
    if retval is None: # some ips are not assigned to a country, only to a continent (usually EU)
        retval = data.continent.code
    return retval
