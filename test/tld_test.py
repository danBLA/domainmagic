# -*- coding: utf-8 -*-
import unittest
import sys
sys.path.insert(0,'../src')
from domainmagic.tld import TLDMagic, get_IANA_TLD_list

class UtilTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_tld_count(self):
        tldmagic = TLDMagic(['com','co.uk','org.co.uk'])
        testdata=[
            ('bla.com',1),
            ('co.uk',2),
            ('bla.co.uk',2),
            ('bla.blubb.co.uk',2),
            ('bla.org.co.uk',3),
        ]
        for fqdn,expectedtldcount in testdata:
            count = tldmagic.get_tld_count(fqdn)
            self.assertEqual(count,expectedtldcount, "Expected TLD count %s from %s, but got %s"%(expectedtldcount,fqdn,count))

    def test_get_tld(self):
        tldmagic = TLDMagic(['com','co.uk','org.co.uk'])
        testdata=[
            ('bla.com','com'),
            ('co.uk','co.uk'),
            ('bla.co.uk','co.uk'),
            ('bla.blubb.co.uk','co.uk'),
            ('bla.org.co.uk','org.co.uk'),
        ]
        for fqdn,expectedtld in testdata:
            tld = tldmagic.get_tld(fqdn)
            self.assertEqual(tld,expectedtld, "Expected TLD %s from %s, but got %s"%(expectedtld,fqdn,tld))
    
    
    def test_get_3tld_no_2tld(self):
        tldmagic = TLDMagic(['com', 'three.ex.com'])
        testdata=[
            ('bla.com','com'),
            ('ex.com','com'),
            ('three.ex.com','three.ex.com'),
            ('bla.three.ex.com','three.ex.com'),
        ]
        for fqdn,expectedtld in testdata:
            tld = tldmagic.get_tld(fqdn)
            self.assertEqual(tld,expectedtld, "Expected TLD %s from %s, but got %s"%(expectedtld,fqdn,tld))
            

    def test_get_domain(self):
        tldmagic = TLDMagic(['com','co.uk','org.co.uk'])
        testdata=[
            ('bla.com','bla.com'),
            ('co.uk','co.uk'),
            ('bla.co.uk','bla.co.uk'),
            ('bla.blubb.co.uk','blubb.co.uk'),
            ('bla.org.co.uk','bla.org.co.uk'),
        ]
        for fqdn,expecteddomain in testdata:
            domain = tldmagic.get_domain(fqdn)
            self.assertEqual(domain,expecteddomain, "Expected Domain %s from %s, but got %s"%(expecteddomain,fqdn,domain))

    def test_fileupdater(self):
        """This test checks if the file update is able to download and set permissions"""
        from domainmagic.fileupdate import FileUpdaterMultiproc
        import tempfile
        import logging
        import os
        import stat

        logging.basicConfig(level=logging.DEBUG)

        fileupdater = FileUpdaterMultiproc()
        filename = tempfile.NamedTemporaryFile(prefix="tlds.", mode="w", dir="/tmp", delete=True).name
        fileupdater.add_file(
            filename,
            "http://data.iana.org/TLD/tlds-alpha-by-domain.txt",
            refresh_time=10,
            minimum_size=1000,
            filepermission=stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IROTH | stat.S_IWOTH)
        fileupdater.wait_for_file(filename, True)

        # make sure file has been created
        self.assertTrue(os.path.exists(filename))

        # now check permissions
        statinfo = os.stat(filename)
        self.assertTrue(statinfo.st_mode & stat.S_IWUSR, "No User write")
        self.assertTrue(statinfo.st_mode & stat.S_IRUSR, "No User read")
        self.assertTrue(statinfo.st_mode & stat.S_IWGRP, "No Group write")
        self.assertTrue(statinfo.st_mode & stat.S_IRGRP, "No Group read")
        self.assertTrue(statinfo.st_mode & stat.S_IROTH, "No Others read")
        self.assertTrue(statinfo.st_mode & stat.S_IWOTH, "No Others write")

    def test_tlsupdate(self):
        """This test checks the default download and permission set"""
        import logging
        import os
        import stat

        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger("domainmagic.test_tlsupdate")

        filename = '/tmp/tlds-alpha-by-domain.txt'
        if os.path.exists(filename):
            logger.debug("File exists already")
            os.remove(filename)
        else:
            logger.debug("File does not exist yet")

        tldlist = get_IANA_TLD_list()
        self.assertTrue(len(tldlist) > 0)

        # make sure file has been created
        self.assertTrue(os.path.exists(filename))

        # now check permissions
        statinfo = os.stat(filename)
        self.assertTrue(statinfo.st_mode & stat.S_IWUSR, "No User write")
        self.assertTrue(statinfo.st_mode & stat.S_IRUSR, "No User read")
        self.assertTrue(statinfo.st_mode & stat.S_IWGRP, "No Group write")
        self.assertTrue(statinfo.st_mode & stat.S_IRGRP, "No Group read")
        self.assertTrue(statinfo.st_mode & stat.S_IROTH, "No Others read")
        self.assertFalse(statinfo.st_mode & stat.S_IWOTH, "No Others write")
